
import React from "react";
import shortid from "shortid";
import "./styles.css";
const content = `
Range is something I discovered recently, 
which again showed me that the possibilities with the 
Javascript and the DOM are truly endless. 
As stated on Mozilla’s developer site, 
range ‘represents a fragment of a document 
that can contain nodes and parts of text nodes’. 
So, if you create of select a section of a document, 
range could tell you the nodes that it contains, 
its starting and ending positions relative to the 
document, it can clone its content ,and much more. 
You can create a range by simply calling 
document.createRange(), 
or you can get the range of a place you selected on a document. 
This is done by calling window.getSelection().getRangeAt(index), 
where ‘index’ is the index of a particular range from an array of ranges.
But lets rewind for a second. How do we get all of the ranges from our selection? 
We get them from window.getSelection(). 
It also gives you a rangeCount, 
which tells you how many ranges the selection contains.
Result of window.getSelection():

Therefore, to get a specific range, 
we call window.getSelection().getRangeAt(index).
Most times, the rangeCount is 1,
so we would get a selections range by calling window.getSelection().getRangeAt(0).
Result of window.getSelection().getRangeAt(0)`;
export default class App extends React.Component {
  state = {
    annnotationObj: {},
    selectionMode: false,
    annotationMode: false,
    selectedText: "",
    startOffset: null,
    endOffset: null
  };

  componentDidMount() {
    this.getRangeFromCoordinates(10, 40, false);
  }

  getRandomColor() {
    var letters = "0123456789ABCDEF";
    var color = "#";
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

  getRangeFromCoordinates = (startOffset, endOffset, extractText, key, action) => {
    const { annnotationObj } = this.state;
    const node = document.getElementById("test");
    let range = new Range();
    range.setStart(node.firstChild, startOffset);
    range.setEnd(node.firstChild, endOffset);
    let charStart = false;
    let charEnd = false;
    if (extractText) {
      this.setState({
        selectedText: range.toString()
      });
    }
    else if (key) {
      switch (action) {
        case 1:
          for (let i = startOffset; i >= 0; i--) {
            let lastChar = content.toString().charAt(i - 1);
            if (lastChar !== " " && !/\s/.test(lastChar)) {
              charStart = true;
            }
            else {
              charEnd = true;
              range.setStart(node.firstChild, i);
              break
            }
          }
          break;
        case 2:
          for (let i = startOffset; i >= 0; i--) {
            let lastChar = content.toString().charAt(i);
            if (lastChar !== " " && !/\s/.test(lastChar)) {
              range.setStart(node.firstChild, i);
              break;
            }
          }
          break;
        case 3:
          for (let i = startOffset; i <= content.length; i++) {
            let lastChar = content.toString().charAt(i);
            if (lastChar !== " " && !/\s/.test(lastChar)) {
              range.setStart(node.firstChild, i);
              break;
            }
          }
          break;
        case 4:
          for (let i = startOffset; i <= content.length; i++) {
            let lastChar = content.toString().charAt(i - 1);
            if (lastChar !== " " && !/\s/.test(lastChar)) {
              charStart = true;
            }
            else {
              charEnd = true;
              range.setStart(node.firstChild, i);
              break
            }
          }
          break;
        case 5:
          for (let i = endOffset; i >= 0; i--) {
            let lastChar = content.toString().charAt(i);
            if (lastChar !== " " && !/\s/.test(lastChar)) {
              charStart = true;
            }
            else {
              charEnd = true;
              range.setEnd(node.firstChild, i);
              break
            }
          }
          break;
        case 6:
          for (let i = endOffset; i >= 0; i--) {
            let lastChar = content.toString().charAt(i - 1);
            if (lastChar !== " " && !/\s/.test(lastChar)) {
              range.setEnd(node.firstChild, i);
              break;
            }
          }
          break;
        case 7:
          for (let i = endOffset; i <= content.length; i++) {
            let lastChar = content.toString().charAt(i - 1);
            if (lastChar !== " " && !/\s/.test(lastChar)) {
              range.setEnd(node.firstChild, i);
              break;
            }
          }
          break;
        case 8:
          for (let i = endOffset; i <= content.length; i++) {
            let lastChar = content.toString().charAt(i);
            if (lastChar !== " " && !/\s/.test(lastChar)) {
              charStart = true;
            }
            else {
              charEnd = true;
              range.setEnd(node.firstChild, i);
              break
            }
          }
          break;

      }
      this.setState({
        selectedText: range.toString()
      });
      const id = key ? key : shortid.generate();
      const boundsFromRange = range.getClientRects();
      const randomColor = this.getRandomColor();
      const newSpanArray = Array.from(boundsFromRange)
        .filter(obj => obj.width > 0)
        .map((domrects, index) => {
          return {
            id: `${id}-${index}`,
            style: {
              left: domrects.x,
              top: domrects.y,
              width: domrects.width,
              height: domrects.height,
              position: "absolute",
              zIndex: 999,
              backgroundColor: randomColor,
              opacity: 0.4,
              border: "3px dotted black"
            }
          };
        });
      this.setState({
        annnotationObj: {
          ...annnotationObj,
          [id]: {
            newSpanArray,
            startOffset: range.startOffset,
            endOffset: range.endOffset
          }
        },
        annotationMode: true,
        selectionMode: false
      });
    }
    else {
      const id = key ? key : shortid.generate();
      const boundsFromRange = range.getClientRects();
      const randomColor = this.getRandomColor();
      const newSpanArray = Array.from(boundsFromRange)
        .filter(obj => obj.width > 0)
        .map((domrects, index) => {
          return {
            id: `${id}-${index}`,
            style: {
              left: domrects.x,
              top: domrects.y,
              width: domrects.width,
              height: domrects.height,
              position: "absolute",
              zIndex: -9,
              backgroundColor: randomColor,
              opacity: 0.4
            }
          };
        });
      this.setState({
        annnotationObj: {
          ...annnotationObj,
          [id]: {
            newSpanArray,
            startOffset: range.startOffset,
            endOffset: range.endOffset
          }
        },
        annotationMode: true,
        selectionMode: false
      });
    }
  };

  annotate = () => {
    const { annnotationObj } = this.state;
    const slection = window.getSelection();
    const range = slection.getRangeAt(0);
    const id = shortid.generate();
    const boundsFromRange = range.getClientRects();
    const randomColor = this.getRandomColor();
    const newSpanArray = Array.from(boundsFromRange)
      .filter(obj => obj.width > 0)
      .map((domrects, index) => {
        return {
          id: `${id}-${index}`,
          style: {
            left: domrects.x,
            top: domrects.y,
            width: domrects.width,
            height: domrects.height,
            position: "absolute",
            zIndex: -9,
            backgroundColor: randomColor,
            opacity: 0.4
          }
        };
      });
    this.setState({
      annnotationObj: {
        ...annnotationObj,
        [id]: {
          newSpanArray,
          startOffset: range.startOffset,
          endOffset: range.endOffset
        }
      },
      annotationMode: true,
      selectionMode: false
    });
  };

  onSpanClick = e => {
    const test = Object.keys(this.state.annnotationObj);
    let targetAnnotation = e.target.id.slice(0, -2);
    this.setState({
      targetAnnotation: targetAnnotation
    });
    let newStateObj = {};
    test.forEach(key => {
      if (key === targetAnnotation) {
        const value = this.state.annnotationObj[key];
        const valueUpdated = value.newSpanArray.map((obj, index) => {
          return {
            id: obj.id,
            style: {
              ...obj.style,
              zIndex: 1000,
            },
            startOffset: value.startOffset,
            endOffset: value.endOffset
          };
        });
        newStateObj[key] = valueUpdated;
      }
    });
    let startOffset = Object.values(newStateObj)[0][0].startOffset;
    let endOffset = Object.values(newStateObj)[0][0].endOffset;
    this.setState({
      startOffset: startOffset,
      endOffset: endOffset
    })
    this.getRangeFromCoordinates(startOffset, endOffset, true);
  };

  selectionMode = () => {
    this.setState({
      annotationMode: false,
      selectionMode: true
    });
  };

  annotationMode = () => {
    this.setState({
      annotationMode: true,
      selectionMode: false
    });
  };

  handleExtension = (action, start) => {
    const { targetAnnotation } = this.state;
    const test = Object.keys(this.state.annnotationObj);
    let newStateObj = {};
    let id = null;
    test.forEach(key => {
      if (key === targetAnnotation) {
        id = key;
        const value = this.state.annnotationObj[key];
        const valueUpdated = value.newSpanArray.map((obj, index) => {
          return {
            id: obj.id,
            style: {
              ...obj.style,
              zIndex: 1000,
            },
          };
        });
        newStateObj[key] = {
          newSpanArray: valueUpdated,
          startOffset: start ? ((action === 1 || action === 2 || action === 5 || action === 6) ? value.startOffset - 1 : value.startOffset + 1) : value.startOffset,
          endOffset: start ? value.endOffset : ((action === 3 || action === 4 || action === 7 || action === 8) ? value.endOffset + 1 : value.endOffset - 1)
        }
      }
    });
    let startOffset = Object.values(newStateObj)[0] && Object.values(newStateObj)[0].startOffset;
    let endOffset = Object.values(newStateObj)[0] && Object.values(newStateObj)[0].endOffset;
    this.getRangeFromCoordinates(startOffset, endOffset, false, id, action);
  }

  render() {
    const {
      annnotationObj,
      selectionMode,
      annotationMode,
      targetAnnotation,
      selectedText
    } = this.state;
    const spanArray = Object.values(annnotationObj);
    return (
      <div className="App">
        <div style={{ display: "flex" }}>
          <div
            style={{
              width: "30%",
              border: "1px solid black",
              borderRight: null,
              padding: 10,
              boxSizing: "border-box"
            }}
          >
            <input type="image" src="https://www.freeiconspng.com/uploads/document-icon-24.png" alt="default" height="100%" width="100%" />
          </div>
          <div
            style={{
              width: "50%",
              border: "1px solid black"
            }}
          >
            <div style={{ display: "flex" }}>
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  borderBottom: "1px solid black",
                  width: "100%",
                  padding: 10,
                  boxSizing: "border-box"
                }}
              >
                <button onClick={this.annotate}>Annotate</button>
                <div style={{ display: "flex" }}>
                  <button
                    onClick={this.selectionMode}
                    style={{ backgroundColor: selectionMode && "blue" }}
                  >
                    Selection
                  </button>
                  <button
                    onClick={this.annotationMode}
                    style={{ backgroundColor: annotationMode && "blue" }}
                  >
                    Annotation
                  </button>
                </div>
              </div>
            </div>
            <pre id="test" style={{ padding: 10, boxSizing: "border-box", whiteSpace: "pre-wrap", display: "inline" }}>
              {content}
              {spanArray.map(valueArray => {
                return valueArray.newSpanArray.map(obj => (
                  <span
                    id={obj.id}
                    style={{
                      ...obj.style,
                      zIndex: selectionMode ? 999 : -999,
                      border: obj.id.includes(targetAnnotation)
                        ? "1px dotted black"
                        : null
                    }}
                    onClick={this.onSpanClick}
                  />
                ));
              })}
              }
            </pre>
          </div>
          <div
            style={{
              width: "20%",
              border: "1px solid black",
              borderLeft: null
            }}
          >
            <div style={{ height: 38, borderBottom: "1px solid black" }} />
            <div style={{ height: 200, borderBottom: "1px solid black", display: "flex", justifyContent: "space-evenly", padding: 10, flexDirection: "column", alignItems: "center" }}>
              <div style={{ display: "flex", width: 100, justifyContent: "space-between", border: "1px solid black", borderRadius: 10, padding: 10, justifySelf: "center", height: "fit-content" }}>
                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQxH4GotSYAnHRFf07s9iYYezzztWKRoSDerQV_h69qMMZDL2ZG&usqp=CAU" onClick={() => this.handleExtension(1, true)} height="20px" width="20px" />
                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTzlRnagAzGRK_At4OpoUYnjjdDu99WSC_ZhP-_rb3cpk52pdRb&usqp=CAU" onClick={() => this.handleExtension(2, true)} height="18px" width="15px" />
                <img src="https://uxwing.com/wp-content/themes/uxwing/download/02-arrow/triangle-right-arrow.png" onClick={() => this.handleExtension(3, true)} height="20px" width="20px" />
                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcS61UfAtRvWGhJBmksnkHR0SummuUtmEF5XQvP2broz0AVo-Zmc&usqp=CAU" onClick={() => this.handleExtension(4, true)} height="18px" width="20px" />
              </div>
              <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRzpWEZhIESWIbuS0ILaTt7A9Pgdx6m9n4bXPl4MHJwuQ_KFqYK&usqp=CAU" height="40px" width="100%" />
              <div style={{ display: "flex", width: 100, justifyContent: "space-between", border: "1px solid black", borderRadius: 10, padding: 10, justifySelf: "center", height: "fit-content" }}>
                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQxH4GotSYAnHRFf07s9iYYezzztWKRoSDerQV_h69qMMZDL2ZG&usqp=CAU" onClick={() => this.handleExtension(5, false)} height="20px" width="20px" />
                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTzlRnagAzGRK_At4OpoUYnjjdDu99WSC_ZhP-_rb3cpk52pdRb&usqp=CAU" onClick={() => this.handleExtension(6, false)} height="18px" width="15px" />
                <img src="https://uxwing.com/wp-content/themes/uxwing/download/02-arrow/triangle-right-arrow.png" onClick={() => this.handleExtension(7, false)} height="20px" width="20px" />
                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcS61UfAtRvWGhJBmksnkHR0SummuUtmEF5XQvP2broz0AVo-Zmc&usqp=CAU" onClick={() => this.handleExtension(8, false)} height="18px" width="20px" />
              </div>
            </div>
            <div style={{ height: 100, borderBottom: "1px solid black" }}>
              <textarea
                id="whiteBoard"
                name="whiteBoard"
                rows="4"
                cols="50"
                style={{ height: 94, width: "95%" }}
                value={selectedText}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
